# mypwr readme.md

# udated: 08Sep22
# author: vk[at]bardwaj[dot]in
# copyleft: vk[at]bardwaj[dot]in

This is a collection of seven files
which are scripts and configuration files

tested on: desktop running Debian GNU/Linux 10 (buster)

Intended to help and support remote access to a desktop 
that is normally off (or hibernating or sleeping)
and was powered on (or resumed working) via some 
external manual or automated action 

Once installed, configured and working,
it updates a log files and sends the log via email 
automatically without human intervention 
date-time stamps of when system came up.

Sends by email a log file of date-time stamp when system came up.

email log entries are triggered via 

* normal power-up, 
* resumption from sleep 
* resumption from hibernation

Log entries merely state date and time of system coming up (or waking up).

Log is emailed via msmtp software

More details :

* script (mypwrlog.sh & mypwrlog-resume.sh) create and update a log file.
* if current log file does NOT exist, create one
* if current log file exists, add date-time-stamp entry in it.
* the log file format is ready for use as-is with msmtp
mail 
* dispatch happens via another separate script (mypwrmail.sh)

After the service and timer files are copied & configured 
use "systemctl" command to enable the services and the timer.

Some (Q&D) confirmatory information
On my working system, here are the seven file locations

* /etc/systemd/system/mypwrlog.service
* /etc/systemd/system/mypwrmail.service
* /etc/systemd/system/mypwrmail.timer
* /usr/lib/systemd/system-sleep/mypwrlog-script.sh
* /usr/local/bin/mypwrlog.sh
* /usr/local/bin/mypwrlog-resume.sh
* /usr/local/bin/mypwrmail.sh

Thanks

created: 31Aug22
author: vk[at]bardwaj[dot]in
copyleft: vk[at]bardwaj[dot]in

