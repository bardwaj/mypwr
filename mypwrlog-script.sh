#!/bin/sh

# mypwrlog-script.sh

# date: 31Aug22
# author: vk[at]bardwaj[dot]in
# copyleft: vk[at]bardwaj[dot]in
# tested on: desktop running Debian GNU/Linux 10 (buster)

# file location /usr/lib/systemd/system-sleep/mypwrup-script.sh

# triggers mypwrlog.service when resuming from sleep or hibernation, 
# which in turn creates-updates log 

case "$1" in
    pre)
            #code execution BEFORE sleeping/hibernating/suspending
    ;;
    post)
            #code execution AFTER resuming
            sudo systemctl start mypwrlog.service
    ;;
esac

exit 0

