#!/bin/bash

# mypwrmail.sh script

# date: 31Aug22
# author: vk[at]bardwaj[dot]in
# copyleft: vk[at]bardwaj[dot]in
# tested on: desktop running Debian GNU/Linux 10 (buster)
# file location: /usr/local/bin/
# description:
# if logs available, and, if mailserver available, then, 
# email *-mypwr.log(s) created by mypwrlog.sh 
# if email successful, then, delete that log file 

# assumptions:
# 1. msmtp is installed, configured and tested working ok
# 2. log(s) were created by mypwrlog.sh and thus suitable for msmtp.
# 3. hostname characters are valid within an email id.

# BEFORE USE, please set the correct values for EMAILTO and MAILSERVER

# initialise variables

SCRIPTNAME="mypwrmail"
COMPUTERNAME=`hostname`
EMAILTO="to.alert@email.id"
MAILSERVER="smtp.server.name"
FILELOCATION="/var/log/"
FILESUFFIX="-mypwr.log"

# processing in a for-do-done loop for each available log file

for FILENAME in $FILELOCATION*$FILESUFFIX; 

do 

# exit if log files NOT available 
if [ ! -f $FILENAME ]; then
   echo "$SCRIPTNAME says NIL log files available to email"
   exit
fi

# exit if mailserver NOT available 
ping -q -w 1 -c 1 $MAILSERVER > /dev/null 
EXITCODE=$?
if [ ! $EXITCODE -eq 0 ]; then
   echo "$SCRIPTNAME says mailserver NOT available"
   exit
fi

# email log file via msmtp 
cat $FILENAME | msmtp $EMAILTO; 
EXITCODE=$?

# delete that log file if email successful
if [ ! $EXITCODE -eq 0 ]; then
   echo "$SCRIPTNAME : $FILENAME email failed"; else
   rm -f $FILENAME;
   echo "$SCRIPTNAME : $FILENAME emailed and deleted"
fi

# wait some time before sending next (pending) log file
sleep 1

done

exit

# end of mypwrmail.sh script


