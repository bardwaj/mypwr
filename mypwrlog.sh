#!/bin/bash

# mypwrlog.sh script

# date: 31Aug22
# author: vk[at]bardwaj[dot]in
# copyleft: vk[at]bardwaj[dot]in
# tested on: desktop running Debian GNU/Linux 10 (buster)
# file location /usr/local/bin/mypwrlog.sh

# if current log file does NOT exist, create one
# if current log file exists, add date-time-stamp entry in it.

# the log file format is ready for use as-is with msmtp
# mail dispatch happens via a separate process (mypwrmail.sh)

# BEFORE USE, please set the correct values for EMAILFROM and EMAILTO 


SCRIPTNAME="mypwrlog"
COMPUTERNAME=`hostname`
CURRENTDATE=`date +"%y%m%d"`
CURRENTTIME=`date +"-%H%M%S"`
EMAILFROM="msmtp.from@email.id"
EMAILTO="to.alert@email.id"
FILELOCATION="/var/log/"
LOGFILESUFFIX="-mypwr.log"

FILENAME=$FILELOCATION$CURRENTDATE$LOGFILESUFFIX

# if current log file does NOT exist, create it with msmtp header
if [ ! -f "$FILENAME" ]; then
echo "To: $EMAILTO" > $FILENAME
echo "From: $EMAILFROM" >> $FILENAME
echo "Subject: $COMPUTERNAME$LOGFILESUFFIX" >> $FILENAME
echo "Message: " >> $FILENAME
echo "$COMPUTERNAME $FILENAME" >> $FILENAME
echo "---" >> $FILENAME
echo "$SCRIPTNAME : $FILENAME created"
fi

# add current line into file
echo "$COMPUTERNAME came up $CURRENTDATE$CURRENTTIME" >> $FILENAME
echo "---" >> $FILENAME
echo "$SCRIPTNAME : $FILENAME updated"
exit

# end of mypwrlog.sh script


